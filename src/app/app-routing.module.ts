import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { NotfoundComponent } from './demo/components/notfound/notfound.component';
import { AppLayoutComponent } from "./layout/app.layout.component";
import { AuthGuard } from './auth.guard'; // Importa tu guard

@NgModule({
    imports: [
        RouterModule.forRoot([
            {   path: '', redirectTo: '/auth/login', pathMatch: 'full'  },
            {   path: 'auth', loadChildren: () => import('./demo/components/auth/auth.module').then(m => m.AuthModule)  },
            {
                path: '', component: AppLayoutComponent, canActivate: [AuthGuard], // Usa el guard para saber si existe una llave con el login.
                children: [
                    { path: 'empty', loadChildren: () => import('./demo/components/pages/empty/emptydemo.module').then(m => m.EmptyDemoModule) },
                    { path: 'pages', loadChildren: () => import('./demo/components/pages/pages.module').then(m => m.PagesModule) }
                ]
            },
            { path: 'notfound', component: NotfoundComponent },
            { path: '**', redirectTo: '/notfound' },
        ], { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled', onSameUrlNavigation: 'reload' })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
