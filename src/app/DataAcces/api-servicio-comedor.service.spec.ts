import { TestBed } from '@angular/core/testing';

import { ApiServicioComedorService } from './api-servicio-comedor.service';

describe('ApiServicioComedorService', () => {
  let service: ApiServicioComedorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiServicioComedorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
