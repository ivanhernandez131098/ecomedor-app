import { PrimeIcons } from 'primeng/api';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiResponse } from './../demo/api/personal'; // Importa ApiResponse

@Injectable({
  providedIn: 'root'
})
export class ApiServicioComedorService {
  // Definir la URL de tu API.
    // QA:
    //private API_URL =  'https://localhost:7245/';

    // Prod:
    private API_URL = 'https://hgtransportaciones.com/comedor/';

  constructor(private http: HttpClient) { }

  // Un ejemplo de un método para obtener datos de la API.
  obtenerDatos(): Observable<any> {
    return this.http.get<any>(`${this.API_URL}GetEmpleado`);
  }

  enviarDatos(codigoBarras: string, idSucursal: number): Observable<ApiResponse> {
    // Construir la URL del endpoint con los valores de código de barras e ID de sucursal
    const apiUrl = `${this.API_URL}GetEmpleado?codigo_barras=${codigoBarras}&id_sucursal=${idSucursal}`;

    // Realizar la solicitud POST al endpoint utilizando la URL construida
    return this.http.post<ApiResponse>(apiUrl, null);
  }

  actualizarDatos(id_personal: number,id_comida: number,idSucursal: number,precio: number): Observable<any> {
    // Construir la URL del endpoint con los valores de código de barras e ID de sucursal
    const apiUrl = `${this.API_URL}Registro_Consumos_Empleado?id_personal=${id_personal}&id_comida=${id_comida}&id_sucursal=${idSucursal}&precio=${precio}`;

    // Realizar la solicitud POST al endpoint utilizando la URL construida
    return this.http.post<any>(apiUrl, null);
  }

  actualizarCorte(idSucursal: number): Observable<any> {
    // Construir la URL del endpoint con los valores de código de barras e ID de sucursal
    const apiUrl = `${this.API_URL}GetCortePendiente?id_sucursal=${idSucursal}`;

    // Realizar la solicitud POST al endpoint utilizando la URL construida
    return this.http.post<any>(apiUrl, null);
  }


}
