import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MessageService, ConfirmationService, Message } from 'primeng/api';
import { HttpClient } from '@angular/common/http';
import { Table } from 'primeng/table';
import { ApiServicioComedorService } from '../../../../DataAcces/api-servicio-comedor.service';
import { DataItem } from '../../../api/corteDiario';
import ConectorPluginV3 from "./../../../../plugins/ConectorPluginV3";


import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';

interface RegistroConsumo {
  cantidad: number;
  id_personal: number;
  nombre: string;
  foto_base64: string;
  precioUnitario: number;
  id_comida: number;
  desc_comida: string;
}

interface expandedRows {
  [key: string]: boolean;
}

@Component({
  templateUrl: './emptydemo.component.html',
  providers: [MessageService],
})
export class EmptyDemoComponent implements OnInit {
  registros: RegistroConsumo[] = [];
  @ViewChild('filter') filter!: ElementRef;
  msgs: Message[] = [];
  impresoraSeleccionada: string = 'EPSON TM-T20III';
  mensaje: string = 'Disfruta tu comida!';
  selectName: string;

  constructor(
    private router: Router,
    private http: HttpClient,
    private service: MessageService,
    private sanitizer: DomSanitizer, // Inyecta DomSanitizer
    private apiServicioComedorService: ApiServicioComedorService
  ) {}

  ngOnInit() {
    // Recuperar el valor almacenado en localStorage
    const selectedBranchId = localStorage.getItem('IdentSucursal');
    const selectedName = localStorage.getItem('IdentSucursalName');


    this.selectName = selectedName;
  }

  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = '';
  }

  guardarRegistro() {
    const nombreInput = document.getElementById('nombreInput') as HTMLInputElement;
    const nombre = nombreInput.value.trim();

    if (nombreInput) {
      if (nombre !== '') {
        // Enviar datos al servicio ApiServicioComedorService
        const codigoBarras = nombre; // Suponiendo que el código de barras está en 'nombre'

        // Recuperar el valor de la sucursal desde localStorage
        const selectedBranchId = localStorage.getItem('IdentSucursal');

        if (!selectedBranchId) {
          // Manejar el caso en el que la sucursal no está presente en localStorage
          this.service.add({
            key: 'tst',
            severity: 'error',
            summary: 'Error en Consumo',
            detail: 'No se encontró la sucursal seleccionada.',
          });
          return;
        }

        // Convertir el valor de la sucursal a un número entero
        const idSucursal = parseInt(selectedBranchId);

        // Llamar al método enviarDatos del servicio
        this.apiServicioComedorService
          .enviarDatos(codigoBarras, idSucursal)
          .subscribe(
            (response) => {
              if (response.dataSingle && response.dataSingle.id_personal) {
                // Verificar si ya existe un registro con el mismo id_personal
                const idPersonal = response.dataSingle.id_personal;
                const registroExistenteIndex = this.registros.findIndex(
                  (registro) => registro.id_personal === idPersonal
                );

                if (registroExistenteIndex !== -1) {
                  // Si ya existe un registro con el mismo id_personal, muestra una alerta y elimina el registro
                  this.service.add({
                    key: 'tst',
                    severity: 'warn',
                    summary: 'Registro Duplicado',
                    detail: 'Ya se ha registrado un consumo para este usuario.',
                  });

                  // Eliminar el registro duplicado de la tabla
                  this.registros.splice(registroExistenteIndex, 1);
                }

                // Crear un nuevo objeto de RegistroConsumo y mapear los datos de ApiResponse
                const nuevoRegistro: RegistroConsumo = {
                  cantidad: 1, // Aquí debes especificar la cantidad adecuada
                  id_personal: idPersonal,
                  foto_base64: response.dataSingle.foto_base64,
                  nombre: response.dataSingle.nombre,
                  id_comida: response.dataSingle.id_comida,
                  desc_comida: response.dataSingle.desc_comida,
                  precioUnitario: response.dataSingle.precio,
                };

                // Agregar el nuevo registro al principio del array de registros
                this.registros.unshift(nuevoRegistro);

                // Limpia el input después de guardar el registro
                nombreInput.value = '';

                // Llamar al método para actualizar los datos, etc.
                this.apiServicioComedorService
                  .actualizarDatos(
                    nuevoRegistro.id_personal,
                    nuevoRegistro.id_comida,
                    idSucursal,
                    nuevoRegistro.precioUnitario
                  )
                  .subscribe(
                    (response) => {
                      if (response.isSucces === true) {
                        // Handle the response for a successful update
                        this.service.add({
                          key: 'tst',
                          severity: 'success',
                          summary: 'Consumo Satisfactorio',
                          detail: 'Se ingresó correctamente el consumo.',
                        });
                      } else {
                        // Check if the message indicates a duplicate entry
                        if (response.message.includes('No es posible registrar el consumo debido a que se exederian los consumos permitidos del dia .')) {
                          this.registros.shift(); // Eliminar el primer elemento (último registro añadido)
                          this.service.add({
                            key: 'tst',
                            severity: 'warn',
                            summary: 'Registro Duplicado',
                            detail: 'Ya se ha registrado un consumo para este usuario.',
                          });
                        } else {
                          this.registros.shift(); // Eliminar el primer elemento (último registro añadido)
                          // Handle other error messages
                          this.service.add({
                            key: 'tst',
                            severity: 'error',
                            summary: 'Error en Consumo',
                            detail: 'No se pudo actualizar el consumo: ' + response.message,
                          });
                        }
                      }
                    },
                    (error) => {
                      // Handle errors from the service
                      this.service.add({
                        key: 'tst',
                        severity: 'error',
                        summary: 'Error en Consumo',
                        detail: 'No se pudo actualizar el consumo: ' + error.message,
                      });
                    }
                  );
              } else {
                // La respuesta de la API no contiene datos válidos
                this.service.add({
                  key: 'tst',
                  severity: 'error',
                  summary: 'Error en Consumo',
                  detail: 'No contiene datos válidos.',
                });
              }
            },
            (error) => {
              // Handle errors from the service
              this.service.add({
                key: 'tst',
                severity: 'error',
                summary: 'Error en Consumo',
                detail: `No se pudo ingresar el consumo. Error: ${error.message}`, // Muestra la descripción del error
              });
            }
          );
      } else {
        // Toast de información incorrecta
        this.service.add({
          key: 'tst',
          severity: 'error',
          summary: 'Error en Consumo',
          detail: 'Ingrese un nombre de producto válido.',
        });
      }
    }
  }



  imprimirUsuario() {
    // Crear un nuevo objeto de imagen
    const imagen = new Image();
    imagen.src =
      'https://hgtransportaciones.com/images/HG-transportaciones-logo-SMALL.png';
    const fechaImpresion = new Date().toLocaleString();

    // Función que se ejecuta cuando la imagen se carga
    imagen.onload = () => {
      // Crear el contenido de la cabecera de la impresión
      const cabeceraHTML = `
        <div>
          <p><strong>Sucursal:</strong> ${ this.selectName } </p>
          <p><strong>Fecha de impresión:</strong> ${fechaImpresion}</p> <!-- Agregamos la fecha y hora de impresión -->
        </div>
      `;

      const contenidoHTML =
        cabeceraHTML + this.generarContenidoDeImpresionUsuarios();

      const printWindow = window.open('', '', 'width=600,height=200');
      printWindow.document.open();
      printWindow.document.write('<html><head><title>Impresión</title></head><body>');
      printWindow.document.write(contenidoHTML);
      printWindow.document.write('</body></html>');
      printWindow.document.close();
      printWindow.print();
      printWindow.close();
      return null;
    };
    return null;
  }

  private generarContenidoDeImpresionUsuarios(): string {
    let contenido = `
      <table>
        <thead>
          <tr>
            <th>Cantidad</th>
            <th>Comida</th>
            <th>Precio</th>
          </tr>
        </thead>
        <tbody>
    `;

    for (const customer of this.registros) {
      contenido += `
        <tr>
          <td>${customer.cantidad}</td>
          <td>${customer.desc_comida}</td>
          <td>${customer.precioUnitario}</td>
        </tr>
      `;
    }

    contenido += `
        </tbody>
      </table>
    `;

    return contenido;
  }

  imprimirCorte() {
    // Recuperar el valor de la sucursal desde localStorage
    const selectedBranchId = localStorage.getItem('IdentSucursal');
    const idSucursal = parseInt(selectedBranchId); // Convertir el valor de la sucursal a un número entero

    this.apiServicioComedorService.actualizarCorte(idSucursal).subscribe(
      (response) => {
        if (response.dataList && response.dataList.length > 0) {
          // Llama a imprimirCorteConLogo pasando response.dataList como argumento
          this.imprimirCorteConLogo(response.dataList);

          // Handle the response from the service if needed
          this.service.add({
            key: 'tst',
            severity: 'success',
            summary: 'Consumo Satisfactorio',
            detail: 'Se ingresó correctamente el consumo.',
          });
        } else {
          // Manejar el caso en que no hay datos en response.dataList
          this.service.add({
            key: 'tst',
            severity: 'warn',
            summary: 'Warn Message',
            detail: 'Ya se realizó un Corte',
          });
          console.log('No hay datos de corte disponibles en response.dataList');
        }
      },
      (error) => {
        // Handle errors from the service
        this.service.add({
          key: 'tst',
          severity: 'error',
          summary: 'Error en Consumo',
          detail: 'No se pudo actualizar el consumo.',
        });
      }
    );
    return null;
  }


  private imprimirCorteConLogo(corte: DataItem[]) {
    // Crear un nuevo objeto de imagen
    const imagen = new Image();
    imagen.src = 'https://hgtransportaciones.com/images/HG-transportaciones-logo-SMALL.png';
    const fechaImpresion = new Date().toLocaleString();

    // Función que se ejecuta cuando la imagen se carga
    imagen.onload = () => {
      // Verificar si document es nulo antes de usarlo
      if (window.document) {
        // Crear el contenido de la cabecera de la impresión
        const cabeceraHTML = `
          <div>
            <img src="https://hgtransportaciones.com/images/HG-transportaciones-logo-SMALL.png" alt="Logo de la empresa" style="width: 300px;">
            <p><strong>Sucursal:</strong> ${this.selectName} </p>
            <p><strong>Fecha de impresión:</strong> ${fechaImpresion} </p> <!-- Agregamos la fecha y hora de impresión -->
            <p><strong>Corte:</strong> ${corte[0].clave_corte}  </p> <!-- Usa corte[0] para obtener la clave del primer elemento -->
          </div>
        `;

        const contenidoHTML = cabeceraHTML + this.generarContenidoDeImpresion(corte);

        const printWindow = window.open('', '', 'width=600,height=600');
        printWindow.document.open();
        printWindow.document.write('<html><head><title>Impresión</title></head><body>');
        printWindow.document.write(contenidoHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
        printWindow.close();
      }
    };
  }



  private generarContenidoDeImpresion(corte: DataItem[]): string {
    // Inicializar las variables para la suma de consecutivos y subtotales
    let cantidadTotal = 0;
    let PrecioTotal = 0;

    // Inicializar la variable para el contenido de la tabla de detalles
    let detalleTablaHTML = `
      <table>
        <thead>
          <tr>
            <th>Consecutivo</th>
            <th>Departamento</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
    `;

    // Iterar a través de los elementos de corte para construir filas de la tabla y sumar los valores
    corte.forEach((item: DataItem) => {
      cantidadTotal += item.consecutivo;
      PrecioTotal += item.subtotal;

      detalleTablaHTML += `
        <tr>
          <td>${item.consecutivo}</td>
          <td>${item.descripcion}</td>
          <td>${item.subtotal}.00</td>
        </tr>
      `;
    });

    // Cerrar la tabla de detalles
    detalleTablaHTML += `
        </tbody>
      </table>
    `;

    // Crear el contenido completo
    const contenido = `
      ${detalleTablaHTML}
      <hr>
      <div class="row" style="display:flex; justify-content:flex-end; justify-content: space-between;">
        <div class="col-6">
          <p style="width:100%">
          <h4 style="display:flex; flex-wrap:no-wrap;">
            <strong style="text-align: right;">Cantidad:</strong> ${cantidadTotal}
          </h4>
          </p>
        </div>
        <div class="col-6">
          <p>
          <h4 style="display:flex; flex-wrap:no-wrap;">
            <strong style="text-align: right;">IVA:</strong> ${0.00}
          </h4>
          </p>
        </div>
      </div>
      <hr>
      <div class="row" style="display:flex; justify-content:flex-end;">
        <p>
        <h3 style="display:flex; flex-wrap:no-wrap;">
          <strong style="text-align: right;">TOTAL:</strong> ${PrecioTotal} </h2>
          </p>
      </div>
    `;

    return contenido;
  }







  async probarImpresion() {
    if (!this.impresoraSeleccionada) {
      return alert("Seleccione una impresora");
    }

    if (!this.mensaje) {
      return alert("Escribe un mensaje");
    }
    const conector = new ConectorPluginV3();
    conector
      .Iniciar()
      .EstablecerAlineacion(ConectorPluginV3.ALINEACION_CENTRO)
      .EscribirTexto("Bienvenido a" + this.selectName )
      .Feed(1)
      .EscribirTexto(this.mensaje)
      .Feed(1)
      .EscribirTexto(this.mensaje = this.generarContenidoDeImpresionUsuarios())
      .DescargarImagenDeInternetEImprimir("https://hgtransportaciones.com/images/HG-transportaciones-logo-SMALL.png", ConectorPluginV3.TAMAÑO_IMAGEN_NORMAL, 200)
      .Iniciar()
      .Feed(1);
    const respuesta = await conector.imprimirEn(this.impresoraSeleccionada);
    if (respuesta == true) {
      console.log("Impresión correcta");
    } else {
      console.log("Error: " + respuesta);
    }
  }


}
