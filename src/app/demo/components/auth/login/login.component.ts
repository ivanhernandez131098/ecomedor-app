import { Component } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Message, MessageService } from 'primeng/api';
import { SelectItem } from 'primeng/api';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [MessageService],
  styles: [`
    :host ::ng-deep .pi-eye,
    :host ::ng-deep .pi-eye-slash {
      transform: scale(1.6);
      margin-right: 1rem;
      color: var(--primary-color) !important;
    }
  `]
})
export class LoginComponent {

  valCheck: string[] = ['remember'];
  user: string = '';
  password: string = '';
  msgs: Message[] = [];
  Sucursales: SelectItem[] = [];
  selectedBranch: any;

  constructor(
    public layoutService: LayoutService,
    private service: MessageService,
    private router: Router,
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.Sucursales = [
      { label: 'HG Transportaciones - Isidoro', value: { id: 16, name: 'HG Transportaciones - Isidoro' } },
      { label: 'HG Transportaciones - Apodaca', value: { id: 7,  name: 'HG Transportaciones - Apodaca' } }
    ];
  }

  login() {
    if (this.user && this.password && this.selectedBranch) {
      const selectedBranchId = this.selectedBranch.id; //Selecciona el valor de la columna id para traer la sucursal.
      const selectedBranchName = this.selectedBranch.name.toString(); //Selecciona el valor de la columna id para traer la sucursal.

      const authData = {
        usuario: this.user,
        contrasena: this.password
      };
      //https://hgtransportaciones.com/security/api/auth/login  //Prod
      //https://localhost:7228/api/Auth/login  //prueba
      this.http.post<any>('https://hgtransportaciones.com/security/api/auth/login', authData)
        .pipe(
        catchError(error => {
            // Aquí puedes manejar el error como quieras
            // Por ejemplo, puedes devolver un Observable con un valor de error personalizado
            return of({ error: 'Hubo un error al hacer la solicitud: ' + error.message });
            })
        )
        .subscribe(
          response => {
            const token = response.token;

            if (token) { // Verifica que el token no sea null
                localStorage.setItem('authToken', token); // Guarda el Token del Login de manera localStorage
                localStorage.setItem('IdentSucursal', selectedBranchId.toString()); // Guarda el id de la sucursal
                localStorage.setItem('IdentSucursalName', selectedBranchName) // Guarda el Nombre de la sucursal

                this.router.navigate(['/empty']);
            } else {
                // Maneja el caso en el que el token es null
                this.msgs = [];
                this.msgs.push({ severity: 'error', summary: 'Error', detail: 'No se encontro este usuario y/o Password' });
            }
          },
          error => {
            this.msgs = [];
            this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Contraseña incorrecta' });
          }
        );
    } else {
      this.msgs = [];
      this.msgs.push({ severity: 'error', summary: 'Error', detail: 'Por favor, ingresa usuario y contraseña.' });
    }
  }
}
