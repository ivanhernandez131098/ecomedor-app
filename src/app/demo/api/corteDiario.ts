export class Corte {
    isSuccess: boolean;
    message: string;
    dataSingle: DataItem;
    dataList: DataItem[];

    constructor() {
      this.isSuccess = false;
      this.message = '';
      this.dataSingle = new DataItem();
      this.dataList = [];
    }
  }

  export class DataItem {
    consecutivo: number;
    descripcion: string;
    subtotal: number;
    iva: number;
    nombre: string;
    no_corte: number;
    fecha_corte: string;
    clave_corte: string;

    constructor() {
      this.consecutivo = 0;
      this.descripcion = '';
      this.subtotal = 0;
      this.iva = 0;
      this.nombre = '';
      this.no_corte = 0;
      this.fecha_corte = '';
      this.clave_corte = '';
    }
  }
