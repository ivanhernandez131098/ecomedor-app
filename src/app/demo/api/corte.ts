export class CorteDiario {

    consecutivo: number;
    descripcion: string;
    subtotal: number;
    iva: number;
    nombre: string;
    no_corte: number;
    fecha_corte: Date;
    clave_corte: string;

    constructor() {
      this.consecutivo = 0;
      this.descripcion = '';
      this.subtotal = 0;
      this.iva = 0;
      this.nombre = '';
      this.no_corte = 0;
      this.fecha_corte = new Date;
      this.clave_corte = '';
    }
}
