export class ApiResponse {
    isSuccess: boolean;
    message: string;
    dataSingle: SingleData;
    dataList: SingleData[];

    constructor() {
      this.isSuccess = false;
      this.message = '';
      this.dataSingle = new SingleData();
      this.dataList = [];
    }
  }

  export class SingleData {
    id_personal: number;
    nombre: string;
    foto_base64: string;
    id_comida: number;
    desc_comida: string;
    precio: number;

    constructor() {
      this.id_personal = 0;
      this.nombre = '';
      this.foto_base64 = '';
      this.id_comida = 0;
      this.desc_comida = '';
      this.precio = 0;
    }
  }
