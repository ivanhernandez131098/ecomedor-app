import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Codbarra } from './../api/codigobarras';

@Injectable({
  providedIn: 'root'
})
export class ComedorService {

  private codigoSource = new BehaviorSubject(new Codbarra());
  unidadActual = this.codigoSource.asObservable();

  constructor() { }

  changeUnidad(unidad: Codbarra) {
    this.codigoSource.next(unidad);
  }
}
